DEMOVIDEO: https://youtu.be/h5Hn1tdkT7Y


JavaSjakk
Dette er et sjakkspill som lar deg dra og slippe brikkene på et 2D-brett med musen. Her kan du spille mot en annen person på samme datamaskin, og man veksler mellom hvem som gjør trekk.

Regler
Reglene for sjakk er de samme som i vanlig sjakk. Målet er å sette motstanderens konge i sjakk matt ved å true den med dine brikker. Du kan flytte brikker ved å dra dem med musen og slippe dem på et gyldig felt (spillet vil ikke la deg flytte brikken til en ugyldig posisjon). Hvis du slipper en brikke på et felt som er okkupert av en motstanders brikke, vil du slå den brikken og ta dens plass.

Hvordan flytte brikkene

Kongen:
 Kan flytte ett felt i hvilken som helst retning (horisontalt, vertikalt eller diagonalt). Den kan ikke flytte til et felt som er truet av en motstanders brikke.

Dronningen:
 Kan flytte så mange felter hun vil i hvilken som helst retning (horisontalt, vertikalt eller diagonalt). Hun kan ikke hoppe over andre brikker.

Tårnet:
 Kan flytte så mange felter det vil horisontalt eller vertikalt. Den kan ikke hoppe over andre brikker.

Løperen:
 Kan flytte så mange felter den vil diagonalt, men kan ikke hoppe over andre brikker.

Springeren:
 Kan flytte to felter horisontalt eller vertikalt og deretter ett felt i en vinkelrett retning. Dette er den eneste brikken som kan hoppe over andre brikker.

Bonden:
 Kan flytte ett felt fremover hvis feltet er tomt. Den kan også flytte to felter fremover hvis den er på sin opprinnelige posisjon og begge feltene er tomme. Den kan slå en motstanders brikke som står på et felt diagonalt foran den.

Hvordan spille
For å starte spillet, må du velge hvilken farge du vil ha på dine brikker (hvit eller svart). Når spillet starter, vil du se et 8x8-brett med brikker plassert på det. Den som har de hvite brikkene begynner først. Du kan flytte en brikke ved å dra den med musen og slippe den på et gyldig felt. Hvis du gjør et ulovlig trekk, vil ikke brikken flytte seg. Dersom du setter motstanderens konge i sjakkmatt er spillet over, og det vil komme en melding på skjermen.