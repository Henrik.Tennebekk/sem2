package no.uib.inf101.sem2.view;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.awt.Color;

import org.junit.jupiter.api.Test;


public class TestDefaultColorTheme {
    @Test
    public void sanityTestDefaultColorTheme() {

        ColorTheme colors = new DefaultColorTheme();

        assertEquals(Color.ORANGE, colors.getCellColor('p'));
        assertEquals(Color.RED, colors.getCellColor('k'));
        assertEquals(Color.GREEN, colors.getCellColor('q'));
        assertEquals(Color.BLUE, colors.getCellColor('r'));
        assertEquals(Color.WHITE, colors.getCellColor('n'));
        assertEquals(Color.MAGENTA, colors.getCellColor('b'));
        assertEquals(new Color(0, 0, 0, 0), colors.getCellColor('-'));
        
        assertEquals(Color.BLACK, colors.getFrameColor());
        assertEquals(Color.WHITE, colors.getTextColor());
        assertEquals(new Color(64, 128, 64, 255), colors.getCheckersColor());
    }
}
