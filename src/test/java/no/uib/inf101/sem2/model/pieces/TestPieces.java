package no.uib.inf101.sem2.model.pieces;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.model.ChessBoard;
import no.uib.inf101.sem2.model.ChessModel;

/**
 * TestPieces
 */
public class TestPieces {

    @Test
    public void testInitialBoard() {
        ChessBoard board = new ChessBoard(8, 8);
        PieceFactory factory = new PieceFactory();
        ChessModel model = new ChessModel(board, factory);

        // Sets the pieces on the board
        model.setInitialBoard();

        // Test if the white pieces are in their initial positions
        assertTrue(board.get(new CellPosition(7, 0)).getPieceType() == 'r');
        assertTrue(board.get(new CellPosition(7, 1)).getPieceType() == 'n');
        assertTrue(board.get(new CellPosition(7, 2)).getPieceType() == 'b');
        assertTrue(board.get(new CellPosition(7, 3)).getPieceType() == 'q');
        assertTrue(board.get(new CellPosition(7, 4)).getPieceType() == 'k');
        assertTrue(board.get(new CellPosition(7, 5)).getPieceType() == 'b');
        assertTrue(board.get(new CellPosition(7, 6)).getPieceType() == 'n');
        assertTrue(board.get(new CellPosition(7, 7)).getPieceType() == 'r');

        for (int i = 0; i < 8; i++) {
            assertTrue(board.get(new CellPosition(6, i)).getPieceType() == 'p');
        }

        // Test if the black pieces are in their initial positions
        assertTrue(board.get(new CellPosition(0, 0)).getPieceType() == 'r');
        assertTrue(board.get(new CellPosition(0, 1)).getPieceType() == 'n');
        assertTrue(board.get(new CellPosition(0, 2)).getPieceType() == 'b');
        assertTrue(board.get(new CellPosition(0, 3)).getPieceType() == 'q');
        assertTrue(board.get(new CellPosition(0, 4)).getPieceType() == 'k');
        assertTrue(board.get(new CellPosition(0, 5)).getPieceType() == 'b');
        assertTrue(board.get(new CellPosition(0, 6)).getPieceType() == 'n');
        assertTrue(board.get(new CellPosition(0, 7)).getPieceType() == 'r');

        for (int i = 0; i < 8; i++) {
            assertTrue(board.get(new CellPosition(1, i)).getPieceType() == 'p');
        }
    }

    @Test
    public void testPawnMovement() {
        ChessBoard board = new ChessBoard(8, 8);
        PieceFactory factory = new PieceFactory();
        ChessModel model = new ChessModel(board, factory);

        // Sets the pieces on the board
        model.setInitialBoard();

        // Test if there is a pawn in a location where it should be
        assertTrue(board.get(new CellPosition(6, 0)).getPieceType() == 'p');

        // Moving white pawn one cell forward
        model.move(new CellPosition(6, 0), new CellPosition(5, 0));
        assertTrue(board.get(new CellPosition(5, 0)).getPieceType() == 'p');

        // Moving black pawn one cell forward
        model.move(new CellPosition(1, 0), new CellPosition(2, 0));
        assertTrue(board.get(new CellPosition(2, 0)).getPieceType() == 'p');

        // Moving white pawn two cells forward on first move
        model.move(new CellPosition(6, 1), new CellPosition(4, 1));
        assertTrue(board.get(new CellPosition(4, 1)).getPieceType() == 'p');

        // Moving black pawn two cells forward on first move
        model.move(new CellPosition(1, 1), new CellPosition(3, 1));
        assertTrue(board.get(new CellPosition(3, 1)).getPieceType() == 'p');

        // Tries doing illegal moves with white pawn
        model.move(new CellPosition(4, 1), new CellPosition(4, 0));
        assertTrue(!(board.get(new CellPosition(4, 0)).getPieceType() == 'p'));
        model.move(new CellPosition(4, 1), new CellPosition(4, 2));
        assertTrue(!(board.get(new CellPosition(4, 2)).getPieceType() == 'p'));
        model.move(new CellPosition(4, 1), new CellPosition(5, 1));
        assertTrue(!(board.get(new CellPosition(5, 1)).getPieceType() == 'p'));

        // Moving white pawn one cell forward
        model.move(new CellPosition(5, 0), new CellPosition(4, 0));
        assertTrue(board.get(new CellPosition(4, 0)).getPieceType() == 'p');

        // Catching white pawn with a black pawn
        model.move(new CellPosition(3, 1), new CellPosition(4, 0));
        assertTrue(board.get(new CellPosition(4, 0)).getTeam() == 'b');
    }


    @Test
    public void testRookMovement() {
        ChessBoard board = new ChessBoard(8, 8);
        PieceFactory factory = new PieceFactory();
        ChessModel model = new ChessModel(board, factory);

        // Sets the pieces on the board
        model.setInitialBoard();

        // Test if there is a rook in a location where it should be
        assertTrue(board.get(new CellPosition(7, 0)).getPieceType() == 'r');

        // Moving pawns to clear the way for the rook
        model.move(new CellPosition(6, 0), new CellPosition(4, 0));
        model.move(new CellPosition(1, 0), new CellPosition(3, 0));


        // Moving white rook forward
        model.move(new CellPosition(7, 0), new CellPosition(5, 0));
        assertTrue(board.get(new CellPosition(5, 0)).getPieceType() == 'r');

        // Moving black rook forward
        model.move(new CellPosition(0, 0), new CellPosition(2, 0));
        assertTrue(board.get(new CellPosition(2, 0)).getPieceType() == 'r');

        // Moving white rook to the right
        model.move(new CellPosition(5, 0), new CellPosition(5, 3));
        assertTrue(board.get(new CellPosition(5, 3)).getPieceType() == 'r');

        // Moving black rook down
        model.move(new CellPosition(2, 0), new CellPosition(2, 3));
        assertTrue(board.get(new CellPosition(2, 3)).getPieceType() == 'r');

        // Tries doing illegal moves with white rook
        model.move(new CellPosition(5, 3), new CellPosition(6, 4));
        assertTrue(!(board.get(new CellPosition(6, 4)).getPieceType() == 'r'));
        model.move(new CellPosition(5, 3), new CellPosition(2, 1));
        assertTrue(!(board.get(new CellPosition(2, 1)).getPieceType() == 'r'));
        model.move(new CellPosition(5, 3), new CellPosition(7, 1));
        assertTrue(!(board.get(new CellPosition(7, 1)).getPieceType() == 'r'));

        // Catching black rook with white rook
        model.move(new CellPosition(5, 3), new CellPosition(2, 3));
        assertTrue(board.get(new CellPosition(2, 3)).getTeam() == 'w');
    }

    
    @Test
    public void testKnightMovement() {
        ChessBoard board = new ChessBoard(8, 8);
        PieceFactory factory = new PieceFactory();
        ChessModel model = new ChessModel(board, factory);

        // Sets the pieces on the board
        model.setInitialBoard();

        // Test if there is a knight in a location where it should be
        assertTrue(board.get(new CellPosition(7, 1)).getPieceType() == 'n');

        // Moving white knight
        model.move(new CellPosition(7, 1), new CellPosition(5, 2));
        assertTrue(board.get(new CellPosition(5, 2)).getPieceType() == 'n');

        // Moving black knight
        model.move(new CellPosition(0, 1), new CellPosition(2, 2));
        assertTrue(board.get(new CellPosition(2, 2)).getPieceType() == 'n');

        // Moving white knight
        model.move(new CellPosition(5, 2), new CellPosition(4, 4));
        assertTrue(board.get(new CellPosition(4, 4)).getPieceType() == 'n');

        // Moving black knight
        model.move(new CellPosition(2, 2), new CellPosition(3, 4));
        assertTrue(board.get(new CellPosition(3, 4)).getPieceType() == 'n');

        // Tries doing illegal moves with white knight
        model.move(new CellPosition(4, 4), new CellPosition(5, 5));
        assertTrue(!(board.get(new CellPosition(5, 5)).getPieceType() == 'n'));
        model.move(new CellPosition(4, 4), new CellPosition(7, 5));
        assertTrue(!(board.get(new CellPosition(7, 5)).getPieceType() == 'n'));
        model.move(new CellPosition(4, 4), new CellPosition(5, 4));
        assertTrue(!(board.get(new CellPosition(5, 4)).getPieceType() == 'n'));

    }

    @Test
    public void testBishopMovement() {
        ChessBoard board = new ChessBoard(8, 8);
        PieceFactory factory = new PieceFactory();
        ChessModel model = new ChessModel(board, factory);

        // Sets the pieces on the board
        model.setInitialBoard();

        // Test if there is a bishop in a location where it should be
        assertTrue(board.get(new CellPosition(7, 2)).getPieceType() == 'b');

        // Moving white pawns to clear the way for the bishop
        model.move(new CellPosition(6, 3), new CellPosition(5, 3));
        model.move(new CellPosition(1, 3), new CellPosition(2, 3));

        // Moving white bishop
        model.move(new CellPosition(7, 2), new CellPosition(4, 5));
        assertTrue(board.get(new CellPosition(4, 5)).getPieceType() == 'b');

        // Moving black bishop
        model.move(new CellPosition(0, 2), new CellPosition(3, 5));
        assertTrue(board.get(new CellPosition(3, 5)).getPieceType() == 'b');

        // Tries doing illegal moves with white bishop
        model.move(new CellPosition(4, 5), new CellPosition(4, 6));
        assertTrue(!(board.get(new CellPosition(4, 6)).getPieceType() == 'b'));
        model.move(new CellPosition(4, 5), new CellPosition(5, 5));
        assertTrue(!(board.get(new CellPosition(5, 5)).getPieceType() == 'b'));
        model.move(new CellPosition(4, 5), new CellPosition(4, 6));
        assertTrue(!(board.get(new CellPosition(4, 6)).getPieceType() == 'b'));
    }

    @Test
    public void testKingMovement() {
        ChessBoard board = new ChessBoard(8, 8);
        PieceFactory factory = new PieceFactory();
        ChessModel model = new ChessModel(board, factory);

        // Sets the pieces on the board
        model.setInitialBoard();

        // Test if there is a king in a location where it should be
        assertTrue(board.get(new CellPosition(7, 4)).getPieceType() == 'k');

        // Moving white pawns to clear the way for the king
        model.move(new CellPosition(6, 4), new CellPosition(4, 4));
        model.move(new CellPosition(1, 4), new CellPosition(3, 4));

        // Moving white king forward
        model.move(new CellPosition(7, 4), new CellPosition(6, 4));
        assertTrue(board.get(new CellPosition(6, 4)).getPieceType() == 'k');

        // Moving white king forward
        model.move(new CellPosition(0, 4), new CellPosition(1, 4));
        assertTrue(board.get(new CellPosition(1, 4)).getPieceType() == 'k');

        // Moving white king diagonally
        model.move(new CellPosition(6, 4), new CellPosition(5, 5));
        assertTrue(board.get(new CellPosition(5, 5)).getPieceType() == 'k');

        // Moving black king diagonally
        model.move(new CellPosition(1, 4), new CellPosition(2, 3));
        assertTrue(board.get(new CellPosition(2, 3)).getPieceType() == 'k');

        // Moving king into catching positions
        model.move(new CellPosition(5, 5), new CellPosition(4, 6));
        model.move(new CellPosition(2, 3), new CellPosition(3, 2));
        model.move(new CellPosition(4, 6), new CellPosition(3, 5));
        model.move(new CellPosition(3, 2), new CellPosition(4, 1));

        // Catching pawn with white king
        model.move(new CellPosition(3, 5), new CellPosition(3, 4));
        assertTrue(board.get(new CellPosition(3, 4)).getPieceType() == 'k');
    }

    @Test
    public void testQueenMovement() {
        ChessBoard board = new ChessBoard(8, 8);
        PieceFactory factory = new PieceFactory();
        ChessModel model = new ChessModel(board, factory);

        // Sets the pieces on the board
        model.setInitialBoard();

        // Test if there is a queen in a location where it should be
        assertTrue(board.get(new CellPosition(7, 3)).getPieceType() == 'q');

        // Moving white pawns to clear the way for the queen
        model.move(new CellPosition(6, 4), new CellPosition(4, 4));
        model.move(new CellPosition(1, 4), new CellPosition(3, 4));

        // Moving white queen diagonally
        model.move(new CellPosition(7, 3), new CellPosition(5, 5));
        assertTrue(board.get(new CellPosition(5, 5)).getPieceType() == 'q');

        // Moving black pawn
        model.move(new CellPosition(1, 3), new CellPosition(3, 3));

        // Testing illegal catch move
        model.move(new CellPosition(5, 5), new CellPosition(3, 3));
        assertTrue(!(board.get(new CellPosition(3, 3)).getPieceType() == 'q'));

        // Testing sideways movement
        model.move(new CellPosition(5, 5), new CellPosition(5, 3));
        assertTrue(board.get(new CellPosition(5, 3)).getPieceType() == 'q');

        // Moving black queen forward
        model.move(new CellPosition(0, 3), new CellPosition(2, 3));
        assertTrue(board.get(new CellPosition(2, 3)).getPieceType() == 'q');

        // Moving black pawn
        model.move(new CellPosition(1, 1), new CellPosition(3, 1));

        // Moving white queen to catch black pawn
        model.move(new CellPosition(5, 3), new CellPosition(3, 3));
        assertTrue(board.get(new CellPosition(3, 3)).getPieceType() == 'q');

        // Moving black queen to catch white queen
        model.move(new CellPosition(2, 3), new CellPosition(3, 3));
        assertTrue(board.get(new CellPosition(3, 3)).getTeam() == 'b');
    }

    @Test
    public void testPieceFactory() {
        ChessBoard board = new ChessBoard(8, 8);
        PieceFactory factory = new PieceFactory();

        board.set(new CellPosition(0, 0), factory.getNext('k', 'w'));
        board.set(new CellPosition(0, 1), factory.getNext('q', 'w'));
        board.set(new CellPosition(0, 2), factory.getNext('r', 'w'));
        board.set(new CellPosition(0, 3), factory.getNext('p', 'w'));
        board.set(new CellPosition(0, 4), factory.getNext('n', 'w'));

        assertTrue(board.get(new CellPosition(0, 0)).getPieceType() == 'k');
        assertTrue(board.get(new CellPosition(0, 1)).getPieceType() == 'q');
        assertTrue(board.get(new CellPosition(0, 2)).getPieceType() == 'r');
        assertTrue(board.get(new CellPosition(0, 3)).getPieceType() == 'p');
        assertTrue(board.get(new CellPosition(0, 4)).getPieceType() == 'n');

    }

}