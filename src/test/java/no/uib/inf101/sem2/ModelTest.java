package no.uib.inf101.sem2;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.model.ChessBoard;
import no.uib.inf101.sem2.model.ChessModel;
import no.uib.inf101.sem2.model.GameState;
import no.uib.inf101.sem2.model.pieces.PieceFactory;

public class ModelTest {

  @Test
  public void testKingPos() {
    ChessBoard board = new ChessBoard(8, 8);
    PieceFactory factory = new PieceFactory();
    ChessModel model = new ChessModel(board, factory);

    // Sets the pieces on the board
    model.setInitialBoard();

    model.move(new CellPosition(6, 5), new CellPosition(4, 5));
    model.move(new CellPosition(1, 4), new CellPosition(3, 4));
    model.move(new CellPosition(6, 6), new CellPosition(4, 6));
    board.set(new CellPosition(4, 7), board.get(new CellPosition(0, 3)));
    board.set(new CellPosition(0, 3), factory.getNext('-', '-'));

    assertEquals(true, model.isCheck(board));
  }


  @Test
  public void testCheckmate() {
    ChessBoard board = new ChessBoard(8, 8);
    PieceFactory factory = new PieceFactory();
    ChessModel model = new ChessModel(board, factory);

    // Sets the pieces on the board
    model.setInitialBoard();

    model.move(new CellPosition(6, 5), new CellPosition(4, 5));
    model.move(new CellPosition(1, 4), new CellPosition(3, 4));
    model.move(new CellPosition(6, 6), new CellPosition(4, 6));
    model.move(new CellPosition(0, 3), new CellPosition(4, 7));

    assertEquals(GameState.GAME_OVER, model.getGameState());
  }


  @Test
  public void testSelfInflictedCheck() {
    ChessBoard board = new ChessBoard(8, 8);
    PieceFactory factory = new PieceFactory();
    ChessModel model = new ChessModel(board, factory);

    // Sets the pieces on the board
    model.setInitialBoard();

    model.move(new CellPosition(6, 4), new CellPosition(4, 4));
    model.move(new CellPosition(1, 4), new CellPosition(3, 4));
    model.move(new CellPosition(7, 3), new CellPosition(3, 7));
    model.move(new CellPosition(1, 5), new CellPosition(2, 5));

    assertEquals(false, board.get(new CellPosition(3, 5)).getPieceType() == 'p');
  }
}
