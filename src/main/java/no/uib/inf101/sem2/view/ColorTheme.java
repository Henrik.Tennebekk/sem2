package no.uib.inf101.sem2.view;
import java.awt.Color;

/**
* Grensesnitt for fargetema
*/
interface ColorTheme {

    /**
     * Finner fargen til en celle
     * @param c en brikketype
     */
    Color getCellColor(Character c);

    /**
     * Finner fargen til rammen
     */
    Color getFrameColor();

    /**
     * Finner fargen til game over boks
     */
    Color getGameOverColor();

    /**
     * Finner tekstfarge
     */
    Color getTextColor();

    /**
     * Finner fargen som danner brettet sammen med hvit
     * @return
     */
    Color getCheckersColor();
}