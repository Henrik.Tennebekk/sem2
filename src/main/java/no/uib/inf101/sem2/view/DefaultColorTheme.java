package no.uib.inf101.sem2.view;

import java.awt.Color;

/**
* Metode for å hente ut fargetema til tetris
* @return Color
*/
public class DefaultColorTheme implements ColorTheme {

    @Override
    public Color getCellColor(Character c) {

        Color color = switch(c) {

            case 'p' -> Color.ORANGE;
            case 'k' -> Color.RED;
            case 'q' -> Color.GREEN;
            case 'r' -> Color.BLUE;
            case 'n' -> Color.WHITE;
            case 'b' -> Color.MAGENTA;
            case '-' -> new Color(0, 0, 0, 0);

            default -> throw new IllegalArgumentException(
                "No available color for '" + c + "'");
        };
        return color;
    }

    @Override
    public Color getFrameColor() {
        return Color.BLACK;
    }

    @Override
    public Color getGameOverColor() {
        return new Color(0, 0, 0, 128);
    }

    @Override
    public Color getTextColor() {
        return Color.WHITE;
    }

    @Override
    public Color getCheckersColor() {
        return new Color(64, 128, 64, 255);
    }
}
