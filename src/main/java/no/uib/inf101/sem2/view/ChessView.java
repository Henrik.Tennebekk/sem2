package no.uib.inf101.sem2.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.model.ChessModel;
import no.uib.inf101.sem2.model.GameState;
import no.uib.inf101.sem2.model.pieces.AbstractPiece;


public class ChessView extends JPanel {

    public ChessModel model;
    public DefaultColorTheme theme;
    private int OUTERMARGIN = 0;

    // Konstruktør
    public ChessView(ChessModel model) {
        this.model = model;
        this.theme = new DefaultColorTheme();
        this.setFocusable(true);
        this.setPreferredSize(new Dimension(800, 800));
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (model.getGameState() == GameState.START_PAGE) {
            drawStartPage(g);
        } else {
            drawCheckersBoard(g);
            drawGame((Graphics2D)g);
        }
    }

    /**
     * Metode som tegner alle brikkene
     * 
     * @param g2 Graphics2D objekt
     * @param grid et Iterable<GridCell<Character>>
     * @param pixelPosition et CellPositionToPixelConverter objekt
     * @param theme et fargetema
     */
    private static void drawCell(Graphics2D g2, Iterable<GridCell<AbstractPiece>> grid, CellPositionToPixelConverter pixelPosition, ColorTheme theme) {
        
        for (GridCell<AbstractPiece> gridCell : grid) {
            
            Rectangle2D cellToDraw = pixelPosition.getBoundsForCell(gridCell.pos());
            Image image = getImageForPiece(gridCell.value());

            if (image != null) {
                g2.drawImage(image, (int) cellToDraw.getX(), (int) cellToDraw.getY(), (int) cellToDraw.getWidth(), (int) cellToDraw.getHeight(), null);
            }
        }
    }  

    /**
     * Metode som finner riktig bildefil for hver brikke
     * Bilder er hentet fra: https://commons.wikimedia.org/wiki/Category:PNG_chess_pieces/Standard_transparent
     * @param piece
     * @return Image
     */
    private static Image getImageForPiece(AbstractPiece piece) {
        if (piece.pieceType != '-') {

            String imageFolder = "./src/main/resources/images/";
            String imagePath = imageFolder + piece.getTeam() + piece.getPieceType() + ".png/";

            try {
                return ImageIO.read(new File(imagePath));
            } catch (Exception IOException) {
                System.out.println("Could not find image: " + imagePath);
            }
        }

        return null;
    }

    
    /**
     * Metode som tegner startskjermen
     */
    private void drawStartPage(Graphics g) {
    // Tegner en rute med spillet inni
        Rectangle2D rectangle = new Rectangle2D.Double(OUTERMARGIN, OUTERMARGIN, this.getWidth() - 2 * OUTERMARGIN , this.getHeight() - 2 * OUTERMARGIN);

        BufferedImage logo = Inf101Graphics.loadImageFromResources("/images/loading_screen.png");
        Inf101Graphics.drawImage(g, logo, rectangle.getX(), rectangle.getY(), 1);

        // Setter fonten og skriver instruks til brukeren
        String startInstructions = "Click the screen to start";

        g.setFont(new Font("Arial", Font.BOLD, 45));
        g.setColor(theme.getTextColor());
        g.drawString(startInstructions, (getWidth() / 2) - (g.getFontMetrics().stringWidth(startInstructions) / 2), getHeight() / 2);

    }


    /**
     * Metode som tegner spillet
     * @param g2 Graphics2D objekt
     */
    private void drawGame(Graphics2D g2) {

        // Tegner en rute med spillet inni
        Rectangle2D rectangle = new Rectangle2D.Double(0, 0, this.getWidth(), this.getHeight());
        CellPositionToPixelConverter posFinder = new CellPositionToPixelConverter(rectangle, model.getDimensions(), 2);

        // Tegner brikkene
        drawCell(g2, model.getTilesOnBoard(), posFinder, theme);


        //Dette tegnes dersom det er game over
        if (model.getGameState() == GameState.GAME_OVER) {

            String gameOver = "Checkmate";
            String winner;

            // Lager en string hvor det står hvem som har vunnet
            if (model.getCurrentTeam() == 'w') {
                winner = "Black wins!";
            } else {
                winner = "White wins!";
            }

            // Tegner en mørk rute over hele spillet
            g2.setColor(theme.getGameOverColor());
            g2.fill(rectangle);

            // Skriver Att det er sjakkmatt
            g2.setFont(new Font("Arial", Font.BOLD, 45));
            g2.setColor(theme.getTextColor());   
            g2.drawString(gameOver, (getWidth() / 2) - (g2.getFontMetrics().stringWidth(gameOver) / 2), getHeight() / 3);
            g2.drawString(winner, (getWidth() / 2) - (g2.getFontMetrics().stringWidth(winner) / 2), getHeight() / 2);
        }
    }

    private void drawCheckersBoard(Graphics g) {

        int sizeX = getWidth();
        int sizeY = getHeight();
        int squareSizeX = sizeX / 8;
        int squareSizeY = sizeY / 8;
        //Makes alternating tiles white/black
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                int x = col * squareSizeX;
                int y = row * squareSizeY;
                if ((row + col) % 2 == 0) {
                    g.setColor(Color.WHITE);
                } else {
                    g.setColor(theme.getCheckersColor());
                }
                g.fillRect(x, y, squareSizeX, squareSizeY);
            }
        }
    }
}

