package no.uib.inf101.sem2.view;

import java.awt.geom.Rectangle2D;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridDimension;

/**
 * Metode som gjør celleposisjoner om til pixelposisjon
 */
public class CellPositionToPixelConverter {

    private Rectangle2D square;
    private GridDimension dimensions;
    private double margin;
  
    public CellPositionToPixelConverter(Rectangle2D square, GridDimension dimensions, double margin) {

        this.square = square;
        this.dimensions = dimensions;
        this.margin = margin;
    }
  
    /**
     * Metode som finner posisjoneringen til en celle
     * @param pos en Cellposition
     * @return Rectangle2D objekt
     */
    public Rectangle2D getBoundsForCell(CellPosition pos) {

        final double squareWidth = square.getWidth();
        final double squareHeight = square.getHeight();
        final double squareX = square.getX();
        final double squareY = square.getY();
        final double cpCol = pos.col();
        final double cpRow = pos.row() ;
        final double dimensionsCols = dimensions.cols();
        final double dimensionsRows = dimensions.rows () ;
        final double cellMargin = margin;

        // Regner ut x og y koordinater samt bredde og høyde
        final double cellWidth = (squareWidth - (cellMargin * (dimensionsCols + 1))) / dimensionsCols;
        final double cellHeight = (squareHeight - (cellMargin * (dimensionsRows + 1))) / dimensionsRows;
        final double cellX = (squareX + (cellMargin * (cpCol + 1)) + (cellWidth * cpCol));
        final double cellY = (squareY + (cellMargin * (cpRow + 1)) + (cellHeight * cpRow));

        // Lager et rektangel ut ifra verdiene som ble regnet ut uvenfor
        return new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeight);
    }
}