package no.uib.inf101.sem2.view;

import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;
import no.uib.inf101.sem2.model.GameState;
import no.uib.inf101.sem2.model.pieces.AbstractPiece;

/**
 * Interface til ViewableTetrisModel
 */
public interface ViewableChessModel {

    /**
     * Metode som returnerer dimensjonene til brettet
     */
    GridDimension getDimensions();

    /**
     * Metode som returnerer rutene på brettet
     */
    Iterable<GridCell<AbstractPiece>> getTilesOnBoard();

    /**
     * Metode som returnerer nåverende gamestate (game over, active game)
     */
    GameState getGameState();
}
