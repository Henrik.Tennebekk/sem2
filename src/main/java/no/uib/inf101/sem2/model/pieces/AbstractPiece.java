package no.uib.inf101.sem2.model.pieces;

public abstract class AbstractPiece {
    public char pieceType;
    public char team;

    /**
     * Metode som returnerer brikketypen til brikken
     * @return char som representerer typen til brikken
     */
    public char getPieceType() {
        return(this.pieceType);
    }

    /**
     * Metode som returnerer "laget" til brikken
     * @return char som representerer laget
     */
    public char getTeam() {
        return(this.team);
    }
}
