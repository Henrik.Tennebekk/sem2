package no.uib.inf101.sem2.model.pieces;

public class Queen extends AbstractPiece {

    public Queen(char team) {
        this.pieceType = 'q';
        this.team = team;
    }
}
