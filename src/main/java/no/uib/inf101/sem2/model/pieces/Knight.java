package no.uib.inf101.sem2.model.pieces;

public class Knight extends AbstractPiece {

    public Knight(char team) {
        this.pieceType = 'n';
        this.team = team;
    }
}
