package no.uib.inf101.sem2.model;

/**
 * "Staten" til spillet
 */
public enum GameState {
    START_PAGE,
    ACTIVE_GAME,
    GAME_OVER
}
