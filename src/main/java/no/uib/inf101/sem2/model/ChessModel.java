package no.uib.inf101.sem2.model;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;
import no.uib.inf101.sem2.model.pieces.AbstractPiece;
import no.uib.inf101.sem2.model.pieces.PieceFactory;
import no.uib.inf101.sem2.view.ViewableChessModel;

public class ChessModel implements ViewableChessModel {

    private ChessBoard board;
    private PieceFactory factory;
    private int turn;
    public GameState gameState;

    public ChessModel (ChessBoard board, PieceFactory factory) {
        this.board = board;
        this.factory = factory;
        this.turn = 0;
        this.gameState = GameState.START_PAGE;
    }

    public void setInitialBoard() {
        // Setter alle Pawns
        for (int i = 0; i < 8; i++) {
            board.set(new CellPosition(1, i), factory.getNext('p', 'b'));
            board.set(new CellPosition(6, i), factory.getNext('p', 'w'));
        }

        // Setter alle Knights
        board.set(new CellPosition(0, 1), factory.getNext('n', 'b'));
        board.set(new CellPosition(0, 6), factory.getNext('n', 'b'));
        board.set(new CellPosition(7, 1), factory.getNext('n', 'w'));
        board.set(new CellPosition(7, 6), factory.getNext('n', 'w'));

        // Setter alle Bishops
        board.set(new CellPosition(0, 2), factory.getNext('b', 'b'));
        board.set(new CellPosition(0, 5), factory.getNext('b', 'b'));
        board.set(new CellPosition(7, 2), factory.getNext('b', 'w'));
        board.set(new CellPosition(7, 5), factory.getNext('b', 'w'));

        // Setter alle Rooks
        board.set(new CellPosition(0, 0), factory.getNext('r', 'b'));
        board.set(new CellPosition(0, 7), factory.getNext('r', 'b'));
        board.set(new CellPosition(7, 0), factory.getNext('r', 'w'));
        board.set(new CellPosition(7, 7), factory.getNext('r', 'w'));

        // Setter alle Queens
        board.set(new CellPosition(0, 3), factory.getNext('q', 'b'));
        board.set(new CellPosition(7, 3), factory.getNext('q', 'w'));

        // Setter alle Kings
        board.set(new CellPosition(0, 4), factory.getNext('k', 'b'));
        board.set(new CellPosition(7, 4), factory.getNext('k', 'w'));
    }



    /**
     * Hjelpemetode som sjekker en rekke/kollonne/diagonal rekke er tom eller ikke
     * 
     * Denne metoden er skrevet delvis med hjelp av AI
     * @return Boolean, true om det er tomt mellom posisjonene, false om ikke
     */
    private boolean isClearTrack(ChessBoard chessBoard, CellPosition initialPos, CellPosition nextPos) {

        int initialRow = initialPos.row();
        int initialCol = initialPos.col();
        int targetRow = nextPos.row();
        int targetCol = nextPos.col();
        int rowIncrement = 0;
        int colIncrement = 0;

        if (initialRow < targetRow) {
            rowIncrement = 1;
        } else if (initialRow > targetRow) {
            rowIncrement = -1;
        }

        if (initialCol < targetCol) {
            colIncrement = 1;
        } else if (initialCol > targetCol) {
            colIncrement = -1;
        }

        int currentRow = initialRow + rowIncrement;
        int currentCol = initialCol + colIncrement;

        while (currentRow != targetRow || currentCol != targetCol) {
            // Check if currentRow and currentCol are within the valid range of indices for your grid
            if (currentRow < 0 || currentRow >= chessBoard.rows() || currentCol < 0 || currentCol >= chessBoard.cols()) {
                // If either currentRow or currentCol is out of bounds, return false
                return false;
            }
            if (chessBoard.get(new CellPosition(currentRow, currentCol)).getPieceType() != '-') return false;
            currentRow += rowIncrement;
            currentCol += colIncrement;
        }

        return true;
    }



    /**
     * Metode som sjekker om en forflyttning er lovlig
     * @param initialPos
     * @param nextPos
     * @return
     */
    private boolean isLegalMove(ChessBoard chessBoard, CellPosition initialPos, CellPosition nextPos, char team) {

        AbstractPiece piece = chessBoard.get(initialPos);

        int initialRow = initialPos.row();
        int initialCol = initialPos.col();
        int targetRow = nextPos.row();
        int targetCol = nextPos.col();

        int colDelta = Math.abs(initialCol - targetCol);
        int rowDelta = Math.abs(initialRow - targetRow);

        // Sjekker at brikken er på riktig lag
        if (piece.getTeam() != team) return false;

        // Ser at det ikke er en tom brikke
        if (piece.pieceType == '-') return false;

        // Sjekker at brikken ikke går utenfor brettet, eller at den lander på noen egne
        if (nextPos.row() < 0 || nextPos.row() > chessBoard.rows()) return false;
        if (nextPos.col() < 0 || nextPos.col() > chessBoard.rows()) return false;
        if (team == chessBoard.get(nextPos).getTeam()) return false;

        // Særregler for Rooks
        if (piece.pieceType == 'r') {
            // Ser at den bare går horisontalt eller vertikalt
            if (initialPos.row() != nextPos.row() && initialPos.col() != nextPos.col()) return false;
            // Ser at den ikke hopper over noen andre brikker
            if (!isClearTrack(chessBoard, initialPos, nextPos)) return false;
        }

        // Særregler for Pawns
        if (piece.pieceType == 'p') {
            if (initialRow == targetRow || colDelta == 1 && rowDelta == 2) return false;

            // Finner ut hvilket lag brikken er på
            if (team == 'w') {
                // Sjekker om det er første forflytningen
                if (initialRow == 6) {
                    if (targetRow < initialRow - 2) return false;
                } else {
                    if (targetRow < initialRow - 1) return false;
                }

                if (chessBoard.get(nextPos).getTeam() == 'b') {
                    if (targetCol > initialCol + 1 || targetCol < initialCol - 1) return false;
                } else {
                    if (targetCol != initialCol) return false;
                }

                if (targetRow > initialRow) return false;
            } else {
                // Sjekker om det er første forflytningen
                if (initialRow == 1) {
                    if (targetRow > initialRow + 2) return false;
                } else {
                    if (targetRow > initialRow + 1) return false;
                }

                if (chessBoard.get(nextPos).getTeam() == 'w') {
                    if (targetCol > initialCol + 1 || targetCol < initialCol - 1) return false;
                } else {
                    if (targetCol != initialCol) return false;
                }

                if (targetRow < initialRow) return false;
            }

            // Sjekker at dem ikke tar noen "rett frem"
            if (targetCol == initialCol && chessBoard.get(nextPos).getPieceType() != '-') return false;
        }

        // Særregler for Kings
        if (piece.pieceType == 'k') {
            if (colDelta > 1|| rowDelta > 1) return false;
        }

        // Særregler for Bishops
        if (piece.pieceType == 'b') {
            if (rowDelta != colDelta) return false;
            if (!isClearTrack(chessBoard, initialPos, nextPos)) return false;
        }

        // Særregler for Knights
        if (piece.pieceType == 'n') {
            if (!(rowDelta == 2 && colDelta == 1) && !(rowDelta == 1 && colDelta == 2)) return false;
        }

        // Særregler for Queens
        if (piece.pieceType == 'q') {

            // Check if the move is along a row, column, or diagonal
            if (rowDelta == 0 || colDelta == 0 || rowDelta == colDelta) {
                // If the move is along a row, column, or diagonal, check if the path is clear
                return isClearTrack(chessBoard, initialPos, nextPos);
            } else {
                // If the move is not along a row, column, or diagonal, it is not a legal move for a queen
                return false;
            }
        }

        return true;
    }



    /**
     * Metode som returnerer en kopi av et sjakkbrett
     * @return ChessBoard
     */
    private ChessBoard getCopiedBoard() {

        ChessBoard copiedBoard = new ChessBoard(8, 8);

        for (int row = 0; row < board.rows(); row++) {
            for (int col = 0; col < board.cols(); col++) {
                CellPosition currentCell = new CellPosition(row, col);
                copiedBoard.set(currentCell, board.get(currentCell));
            }
        }

        return copiedBoard;
    }



    /**
     * Metode som returnerer char som representerer laget som spiller
     * @return char
     */
    public char getCurrentTeam() {
        if (turn % 2 == 0) return 'w';
        return 'b';
    }



    /**
     * Metode som returnerer char som representerer motstanderlaget
     * @return char
     */
    private char getOppositeTeam() {
        if (turn % 2 == 0) return 'b';
        return 'w';
    }



    /**
     * Metode som returnerer CellPosition til kongen på et av lagene
     * @param team
     * @return CellPosition, posisjonen til kongen
     */
    private CellPosition getKing(ChessBoard chessBoard, char team) {
        for (int x = 0; x < chessBoard.rows(); x++) {
            for (int y = 0; y < chessBoard.cols(); y++) {
                CellPosition position = new CellPosition(x, y);
                if (chessBoard.get(position).getPieceType() == 'k' && chessBoard.get(position).getTeam() == team) {
                    return new CellPosition(x, y);
                }
            }
        }
        return null; // returnerer null om ingen er funnet
    }



    /**
     * Metode som sier om et lag er i sjakk
     * @param ChessBoard brettet
     * @param Cellposition posisjonen til kongen
     * @return boolean
     */
    public boolean isCheck(ChessBoard chessBoard) {
        for (int row = 0; row < chessBoard.rows(); row++) {
            for (int col = 0; col < chessBoard.cols(); col++) {

                CellPosition position = new CellPosition(row, col);

                if (isLegalMove(chessBoard, position, getKing(chessBoard, getOppositeTeam()), getCurrentTeam())) {
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * Metode som sjekker om man selv kommer i sjakk når man gjør et trekk
     * @param initialPos
     * @param nextPos
     * @return
     */
    private boolean isInCheck(CellPosition initialPos, CellPosition nextPos) {

        for (int row = 0; row < board.rows(); row++) {
            for (int col = 0; col < board.cols(); col++) {
                ChessBoard boardCopy = getCopiedBoard();

                boardCopy.set(nextPos, board.get(initialPos));
                boardCopy.set(initialPos, factory.getNext('-', '-'));

                CellPosition position = new CellPosition(row, col);

                if (isLegalMove(boardCopy, position, getKing(boardCopy, getCurrentTeam()), getOppositeTeam())) {
                    return true;
                }
            }
        }
        return false;
    }



    /**
     * Metode som sjekker om det er sjakkmatt
     * @return boolean
     */
    private boolean isCheckmate(ChessBoard chessBoard, CellPosition kingPos) {
        if (!isCheck(chessBoard)) return false; // Returnerer false om det ikke er sjakk i det hele tatt

        for (int row = 0; row < chessBoard.rows(); row++) {
            for (int col = 0; col < chessBoard.cols(); col++) {
                
                for (int i = 0; i < chessBoard.rows(); i++) {
                    for (int j = 0; j < chessBoard.cols(); j++) {

                        // Lager en kopi av sjakkbrettet
                        ChessBoard tempBoard = getCopiedBoard();

                        // Endrer brettet med scenarioene den skal teste for sjakk
                        CellPosition initialPos  = new CellPosition(row, col);
                        CellPosition nextPos  = new CellPosition(i, j);

                        if (isLegalMove(tempBoard, initialPos, nextPos, getOppositeTeam())) {
                            tempBoard.set(nextPos, board.get(initialPos));
                            tempBoard.set(initialPos, factory.getNext('-', '-'));

                            if (!isCheck(tempBoard)) {
                                System.out.println("Check detected, can be averted this way:");
                                outputBoard(tempBoard);
                                return false; // Fare ungått
                            }
                        }
                    }
                }
            }
        }   
        return true; // Sjakk matt
    }



    /**
     * Metode som flytter på en brikke
     * @param initialPos
     * @param nextPos
     * @return boolean, true om lovlig move, false ellers
     */
    public void move(CellPosition initialPos, CellPosition nextPos) {
        System.out.println("\n________________________________________________________________");
        System.out.println("New Move:");
        System.out.println("Current team: " + getCurrentTeam());
        System.out.println("Moving from " + initialPos.row() + "," + initialPos.col() + " to " + nextPos.row() + "," + nextPos.col());

        if (isLegalMove(board, initialPos, nextPos, getCurrentTeam()) && !isInCheck(initialPos, nextPos)) {
            board.set(nextPos, board.get(initialPos));
            board.set(initialPos, factory.getNext('-', '-'));
            // Sjekker om det er sjakk
            if (isCheckmate(board, getKing(board, getOppositeTeam()))) this.gameState = GameState.GAME_OVER;
            this.turn++;
        } else {
            System.out.println("Illegal move, not completed");
        }
    }



    /**
     * Metode som brukes for å skrive ut brettet i terminalen
     */
    private void outputBoard(ChessBoard chessBoard) {

        for (int i = 0; i < chessBoard.rows(); i++) {
            System.out.print("\n");
            for (int j = 0; j < chessBoard.cols(); j++) {
                System.out.print(chessBoard.get(new CellPosition(i, j)).getPieceType());
            }
        }
    }

    @Override
    public GridDimension getDimensions() {
        return board;
    }

    @Override
    public Iterable<GridCell<AbstractPiece>> getTilesOnBoard() {
        return board;
    }

    @Override
    public GameState getGameState() {
        return this.gameState;
    }
}
