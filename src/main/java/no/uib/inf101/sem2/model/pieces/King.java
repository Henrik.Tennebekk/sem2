package no.uib.inf101.sem2.model.pieces;

public class King extends AbstractPiece {

    public King(char team) {
        this.pieceType = 'k';
        this.team = team;
    }
}

