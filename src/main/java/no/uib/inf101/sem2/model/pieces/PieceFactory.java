package no.uib.inf101.sem2.model.pieces;

public class PieceFactory implements IPieceFactory {
    
    public PieceFactory() {
    
    }

    /**
     * Metode som produserer ny brikke
     * @param pieceType Bestemmer hvilken type brikke den skal produsere
     * @param team Bestemmer om brikken er hvit eller svart
     * @return AbstractPiece En brikke
     */
    @Override
    public AbstractPiece getNext(char pieceType, char team) {
        if (pieceType == 'p') return new Pawn(team);
        if (pieceType == 'k') return new King(team);
        if (pieceType == 'q') return new Queen(team);
        if (pieceType == 'r') return new Rook(team);
        if (pieceType == 'n') return new Knight(team);
        if (pieceType == 'b') return new Bishop(team);
        else {
            return new Empty('-'); // Returnerer fordi den må
        }
    }
}
