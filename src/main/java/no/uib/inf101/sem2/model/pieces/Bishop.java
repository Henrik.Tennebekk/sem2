package no.uib.inf101.sem2.model.pieces;

public class Bishop extends AbstractPiece {

    public Bishop(char team) {
        this.pieceType = 'b';
        this.team = team;
    }
}
