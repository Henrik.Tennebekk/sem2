package no.uib.inf101.sem2.model.pieces;

public interface IPieceFactory {
    
    /**
     * Metode som returnerer ny brikke
     * @param pieceType Char som representerer brikketype
     * @return AbstractPiece en brikke
     */
    public AbstractPiece getNext(char pieceType, char team);
}
