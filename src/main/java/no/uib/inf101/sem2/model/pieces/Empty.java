package no.uib.inf101.sem2.model.pieces;

public class Empty extends AbstractPiece {

    public Empty(char team) {
        this.pieceType = '-';
        this.team = '-';
    }
}
