package no.uib.inf101.sem2.model.pieces;

public class Rook extends AbstractPiece {

    public Rook(char team) {
        this.pieceType = 'r';
        this.team = team;
    }
}
