package no.uib.inf101.sem2.model.pieces;

public class Pawn extends AbstractPiece {

    public Pawn(char team) {
        this.pieceType = 'p';
        this.team = team;
    }
}
