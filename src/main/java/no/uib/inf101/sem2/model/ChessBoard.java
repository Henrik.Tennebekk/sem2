package no.uib.inf101.sem2.model;

import no.uib.inf101.sem2.grid.Grid;
import no.uib.inf101.sem2.model.pieces.AbstractPiece;
import no.uib.inf101.sem2.model.pieces.Empty;


/**
 * Et sjakkbrett (objekt) som utvider Grid<AbstractPiece>
 */
public class ChessBoard extends Grid<AbstractPiece> {
    public ChessBoard(int row, int col) {
        super(row, col, new Empty('-'));
    }
}
