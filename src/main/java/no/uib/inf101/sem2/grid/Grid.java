package no.uib.inf101.sem2.grid;

import java.util.Iterator;
import java.util.ArrayList;

public class Grid<E> implements IGrid<E> {

    private int row;
    private int col;
    private ArrayList<ArrayList<E>> grid;

    /**
     * Konstruktør 1
     * @param row
     * @param col
     * @param initialValue
     */
    public Grid(int row, int col, E initialValue) {
        this.row = row;
        this.col = col;
        
        grid = new ArrayList<ArrayList<E>>();

        for (int i = 0; i < row; i++) {
            ArrayList<E> rowList = new ArrayList<E>();
            for (int j = 0; j < col; j++) {
              rowList.add(initialValue);
            }
            grid.add(rowList);
          }
    }

    /**
     * Konstruktør 2, bruker andre konstruktøren for å lage et grid, setter verdiene i grid til null
     * @param row
     * @param col
     */
    public Grid(int row, int col) {
        this(row, col, null);
    }

    @Override
    public int rows() {
        return this.row;
    }

    @Override
    public int cols() {
        return this.col;
    }

    @Override
    public Iterator<GridCell<E>> iterator() {
        ArrayList<GridCell<E>> iterator = new ArrayList<>();

        for (int y = 0; y < row; y++) {
            for (int x = 0; x < col; x++) {
                CellPosition pos = new CellPosition(y, x);
                GridCell<E> gridItem = new GridCell<E>(pos, get(pos));

                iterator.add(gridItem);
            }
        }
        return iterator.iterator();
    }

    @Override
    public void set(CellPosition pos, E value) {
        if (!positionIsOnGrid(pos)) {
            throw new IndexOutOfBoundsException("Position is out of bounds");
        }
        grid.get(pos.row()).set(pos.col(), value);
    }

    @Override
    public E get(CellPosition pos) {
        if (!positionIsOnGrid(pos)) {
            throw new IndexOutOfBoundsException("Position is out of bounds");
        }
        return grid.get(pos.row()).get(pos.col());
    }

    @Override
    public boolean positionIsOnGrid(CellPosition pos) {
        // Returnerer true dersom posisjonen er innenfor grid, returnerer false hvis ikke
        return pos.row() >= 0 && pos.row() < this.rows() && pos.col() >= 0 && pos.col() < this.cols();
    }
}
