package no.uib.inf101.sem2.grid;

/**
* Oppbevarer en posisjonsverdi
* @param row raden / y-verdien
* @param col kolonnen / x-verdien
*/
public record CellPosition(int row, int col) {}