package no.uib.inf101.sem2.grid;

/**
* Stores the value of a cell
* @param pos the position in which to store the value
* @param value the new value
*/
public record GridCell<E>(CellPosition pos, E value) {}