package no.uib.inf101.sem2.grid;

public interface GridDimension {

  /**
   * Returnerer antall rader i et grid
   * @return int
   */
  int rows();

  /**
   * Returnerer antall kollonner i et grid
   * @return int
   */
  int cols();
}
