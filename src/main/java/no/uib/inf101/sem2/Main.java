package no.uib.inf101.sem2;

import no.uib.inf101.sem2.controller.ChessController;
import no.uib.inf101.sem2.model.ChessBoard;
import no.uib.inf101.sem2.model.ChessModel;
import no.uib.inf101.sem2.model.pieces.PieceFactory;
import no.uib.inf101.sem2.view.ChessView;

import javax.swing.JFrame;

public class Main {
  public static void main(String[] args) {
    ChessBoard board = new ChessBoard(8, 8);
    PieceFactory factory = new PieceFactory();

    ChessModel model = new ChessModel(board, factory);
    model.setInitialBoard();

    ChessView view = new ChessView(model);
    new ChessController(model, view);

    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("Chess");
    frame.setContentPane(view);
    frame.pack();
    frame.setVisible(true);
  }
}
