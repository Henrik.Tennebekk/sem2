package no.uib.inf101.sem2.controller;

import no.uib.inf101.sem2.model.GameState;

public interface ControllableChessModel {
    
    /**
     * Metode som returnerer GameState
     * @return GameState
     */
    public GameState getGameState();

}
