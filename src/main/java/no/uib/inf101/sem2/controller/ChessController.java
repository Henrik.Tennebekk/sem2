package no.uib.inf101.sem2.controller;

import java.awt.event.MouseEvent;
import javax.swing.event.MouseInputListener;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.model.ChessModel;
import no.uib.inf101.sem2.model.GameState;
import no.uib.inf101.sem2.view.ChessView;

public class ChessController implements ControllableChessModel, MouseInputListener {

    ChessModel model;
    ChessView view;
    private CellPosition initialPos;

    // Konstruktøren til kontrolleren
    public ChessController(ChessModel model, ChessView view){
        this.model = model;
        this.view = view;
        this.view.addMouseListener(this);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (model.getGameState() == GameState.START_PAGE) model.gameState = GameState.ACTIVE_GAME;
    }

    @Override
    public void mousePressed(MouseEvent e) {

        int gridX = (int) e.getX() / (view.getWidth() / 8);
        int gridY = (int) e.getY() / (view.getHeight() / 8);
        this.initialPos = new CellPosition(gridY, gridX);
    }

    @Override
    public void mouseReleased(MouseEvent e) {

        int gridX = (int) e.getX() / (view.getWidth() / 8);
        int gridY = (int) e.getY() / (view.getHeight() / 8);
        model.move(initialPos, new CellPosition(gridY, gridX));
        view.repaint();
    }
    
    
    @Override
    public GameState getGameState() {
        return model.getGameState();
    }

    @Override public void mouseEntered(MouseEvent e) {}
    @Override public void mouseExited(MouseEvent e) {}
    @Override public void mouseDragged(MouseEvent e) {}
    @Override public void mouseMoved(MouseEvent e) {}
}
